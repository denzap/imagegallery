package com.deniszaplatnikov.imagegallery.ui.imageDetails;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.deniszaplatnikov.imagegallery.R;
import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.deniszaplatnikov.imagegallery.ui.utils.ForceLocaleRule;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Locale;

import static org.hamcrest.Matchers.not;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;

@RunWith(AndroidJUnit4.class)
public class ImageDetailsActivityTest {

    @ClassRule public static final ForceLocaleRule localeTestRule = new ForceLocaleRule(new Locale("ru", "RU"));
    @Rule public ActivityTestRule<ImageDetailsActivity> activityTestRule = new ActivityTestRule<ImageDetailsActivity>(ImageDetailsActivity.class) {
        @Override
        protected Intent getActivityIntent() {
            Intent intent = new Intent();
            intent.putExtra(ImageDetailsActivity.EXTRA_IMAGE_DETAILS, imageResource);
            return intent;
        }
    };
    private String imageName = "cat.jpg";
    private String originalUrl = "https://downloader.disk.yandex.ru/disk/c52bfa5e816690a3b91fa4f811bf82e6cd1067a692e31fde8f6a2338f4698433/5aebad4f/H7xROOCyjZKkKYvjDR42VzVwBVMRmSmvZqxZBTTUFdeIdTyy-9E03e9B1wwNxZun-2WC1U1V9CixopnPdmGHIg%3D%3D?uid=0&filename=cat.jpg&disposition=inline&hash=&limit=0&content_type=image%2Fjpeg&fsize=496464&hid=082a961816dfa432eb3224ea6fc20e78&media_type=image&tknv=v2&etag=1a900654f710d4c8a05a34af6337e085";
    private String shortUrl = "shortUrl";
    private String created = "2018-04-06T12:50:01+00:00";
    private ImageResource imageResource = new ImageResource(imageName, originalUrl, shortUrl, created, 496464);

    @Test
    public void toolbarInitialSetup() {
        onView(withId(R.id.toolbar_title)).check(matches(withText(R.string.view)));
        onView(withId(R.id.right_item_text)).check(matches(not(isDisplayed())));
        onView(withId(R.id.right_item_image)).check(matches(isDisplayed()));
    }

    @Test
    public void initialImageViewLayout() {
        onView(withId(R.id.image)).check(matches(isDisplayed()));

        onView(withId(R.id.reload_image)).check(matches(not(isDisplayed())));
        onView(withId(R.id.reload_image_layout)).check(matches(not(isDisplayed())));

        checkImageInfoSetUp();
    }

    private void checkImageInfoSetUp() {
        onView(withId(R.id.text_image_name)).check(matches(
                withText(String.format("%s: %s", activityTestRule.getActivity().getResources().getString(R.string.image_name), imageName))));
        onView(withId(R.id.text_image_made_time)).check(matches(
                withText(String.format("%s: %s", activityTestRule.getActivity().getResources().getString(R.string.image_created), "06 апреля 2018, 15:50"))));
        onView(withId(R.id.text_image_size)).check(matches(
                withText(String.format("%s: %s", activityTestRule.getActivity().getResources().getString(R.string.image_size), "484,83 КБ"))));
    }
}