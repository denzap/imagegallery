package com.deniszaplatnikov.imagegallery.ui.imageDetails;

import android.support.annotation.NonNull;

import com.deniszaplatnikov.imagegallery.ui.base.BasePresenter;
import com.deniszaplatnikov.imagegallery.ui.base.BaseView;
import com.deniszaplatnikov.imagegallery.data.ImageResource;

public interface ImageDetailsContract {

    interface View extends BaseView {

        /**
         * Загрузка и отображение изображения полученного из данного url.
         * @param url для загрузки.
         */
        void loadImage(String url);

        /**
         * Установка времени создания изображения.
         * @param time текстовое представление времени для отображения.
         */
        void setImageMadeTime(String time);

        /**
         * Установка названия изображения.
         * @param name название изображения.
         */
        void setImageName(String name);

        /**
         * Установка размера изображения.
         * @param size текстовое представление размера для отображения.
         */
        void setImageSize(String size);

        /**
         * Показать или спрятать экран загрузки.
         * @param show true, чтобы показать, false, чтобы спрятать.
         */
        void showProgressView(boolean show);

        /**
         * Начать анимацию перезагрузки изображения.
         */
        void startReloadImageAnimation();

        /**
         * Остановить анимацию перезагрузки изображения.
         */
        void stopReloadImageAnimation();

        /**
         * Показать или спрятать экран информации об изображении.
         * @param show true, чтобы показать, false, чтобы спрятать.
         */
        void showImageDetailsView(boolean show);

        /**
         * Показать или спрятать само изображение.
         * @param show true, чтобы показать, false, чтобы спрятать.
         */
        void showImage(boolean show);

        /**
         * Показать или спрятать информацию том,
         * что загрузка изображения была неуспешной и стоит повторить попытку.
         * @param show true, чтобы показать, false, чтобы спрятать.
         */
        void showLoadImageError(boolean show);

        /**
         * Инициировать переход на загрузку изображения.
         * @param url для загрузки изображения.
         */
        void saveImage(String url);
    }

    interface Presenter extends BasePresenter<View> {

        /**
         * Дать презентеру информацию о текущем изображение.
         * @param imageDetails информацию об изображении.
         */
        void setImageDetails(@NonNull ImageResource imageDetails);

        /**
         * Оповещение о том, что изображение было загружено.
         * @param success результат загрузки.
         */
        void imageLoaded(boolean success);

        /**
         * Необходимо сохранить изображение.
         */
        void onSaveImageClicked();

        /**
         * Необходимо перезагрузить изображение.
         */
        void onReloadImageClicked();
    }
}
