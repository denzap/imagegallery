package com.deniszaplatnikov.imagegallery.ui.gallery;

import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.deniszaplatnikov.imagegallery.di.test.DaggerTestNetworkComponent;
import com.deniszaplatnikov.imagegallery.di.test.TestNetworkComponent;
import com.deniszaplatnikov.imagegallery.di.test.TestNetworkModule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import io.reactivex.schedulers.Schedulers;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import timber.log.Timber;

import static org.junit.Assert.*;

public class GalleryModelTest {

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    private MockWebServer server;
    private GalleryModel galleryModel;

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();

        TestNetworkComponent testNetworkComponent = DaggerTestNetworkComponent.builder()
                .testNetworkModule(new TestNetworkModule(server.url("/").toString()))
                .build();
        galleryModel = new GalleryModel(testNetworkComponent.getYandexDiskService());
    }

    @Test
    public void reloadImageResourceWithMaxLimitResourcesResponse() {
        String get20ResourcesSuccess = getStringFromFile( "get_20_resources_success.json");
        MockResponse mockResponseGet20ResourcesSuccess = new MockResponse().setResponseCode(200).setBody(get20ResourcesSuccess);
        server.enqueue(mockResponseGet20ResourcesSuccess);

        galleryModel.reloadImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();
        assertEquals(20, galleryModel.getImageResourcesCount());
        assertEquals(true, galleryModel.hasAnyResourcesToLoadMore());
    }

    @Test
    public void reloadImageResourceWithLessThenMaxLimitResourcesResponse() {
        String getSingleResourcesSuccess = getStringFromFile( "get_single_resource_success.json");
        MockResponse mockResponseGetSingleResourcesSuccess = new MockResponse().setResponseCode(200).setBody(getSingleResourcesSuccess);
        server.enqueue(mockResponseGetSingleResourcesSuccess);

        galleryModel.reloadImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();
        assertEquals(1, galleryModel.getImageResourcesCount());
        assertEquals(false, galleryModel.hasAnyResourcesToLoadMore());
    }

    @Test
    public void reloadImageResourceWithEmptyResourcesResponse() {
        String getEmptyResourcesSuccess = getStringFromFile( "get_empty_resources_success.json");
        MockResponse mockResponseGetEmptyResourcesSuccess = new MockResponse().setResponseCode(200).setBody(getEmptyResourcesSuccess);
        server.enqueue(mockResponseGetEmptyResourcesSuccess);

        galleryModel.reloadImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();
        assertEquals(0, galleryModel.getImageResourcesCount());
        assertEquals(false, galleryModel.hasAnyResourcesToLoadMore());
    }

    @Test
    public void loadMoreImageResourceWithMaxLimitResourcesResponse() {
        String get20ResourcesSuccess = getStringFromFile( "get_20_resources_success.json");
        MockResponse mockResponseGet20ResourcesSuccess = new MockResponse().setResponseCode(200).setBody(get20ResourcesSuccess);
        server.enqueue(mockResponseGet20ResourcesSuccess);

        galleryModel.loadMoreImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();
        assertEquals(20, galleryModel.getImageResourcesCount());
        assertEquals(true, galleryModel.hasAnyResourcesToLoadMore());
    }

    @Test
    public void loadMoreImageResourceWithLessThenMaxLimitResourcesResponse() {
        String getSingleResourcesSuccess = getStringFromFile( "get_single_resource_success.json");
        MockResponse mockResponseGetSingleResourcesSuccess = new MockResponse().setResponseCode(200).setBody(getSingleResourcesSuccess);
        server.enqueue(mockResponseGetSingleResourcesSuccess);

        galleryModel.loadMoreImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();
        assertEquals(1, galleryModel.getImageResourcesCount());
        assertEquals(false, galleryModel.hasAnyResourcesToLoadMore());
    }

    @Test
    public void loadMoreImageResourceWithEmptyResourcesResponse() {
        String getEmptyResourcesSuccess = getStringFromFile( "get_empty_resources_success.json");
        MockResponse mockResponseGetEmptyResourcesSuccess = new MockResponse().setResponseCode(200).setBody(getEmptyResourcesSuccess);
        server.enqueue(mockResponseGetEmptyResourcesSuccess);

        galleryModel.loadMoreImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();
        assertEquals(0, galleryModel.getImageResourcesCount());
        assertEquals(false, galleryModel.hasAnyResourcesToLoadMore());
    }

    @Test
    public void imagesStoredWhenLoadMore() {
        String get20ResourcesSuccess = getStringFromFile( "get_20_resources_success.json");
        String getSingleResourcesSuccess = getStringFromFile( "get_single_resource_success.json");
        MockResponse mockResponseGetSingleResourcesSuccess = new MockResponse().setResponseCode(200).setBody(getSingleResourcesSuccess);
        MockResponse mockResponseGet20ResourcesSuccess = new MockResponse().setResponseCode(200).setBody(get20ResourcesSuccess);
        server.enqueue(mockResponseGetSingleResourcesSuccess);
        server.enqueue(mockResponseGet20ResourcesSuccess);

        galleryModel.reloadImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();

        galleryModel.loadMoreImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();

        assertEquals(21, galleryModel.getImageResourcesCount());
        assertEquals(false, galleryModel.hasAnyResourcesToLoadMore());
    }

    @Test
    public void getResourcesWithValidFields() {
        String getSingleResourcesSuccess = getStringFromFile( "get_single_resource_success.json");
        MockResponse mockResponseGetSingleResourcesSuccess = new MockResponse().setResponseCode(200).setBody(getSingleResourcesSuccess);
        server.enqueue(mockResponseGetSingleResourcesSuccess);

        galleryModel.reloadImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();

        ImageResource imageResource = galleryModel.getImageResource(0);
        assertEquals("cat.jpg", imageResource.getName());
        assertEquals("https://downloader.disk.yandex.ru/disk/a5e440124532fc8f73d5dfff18eee42aa460ce1251f583760451ec4ddbbea7e0/5ae9f9e0/H7xROOCyjZKkKYvjDR42VzVwBVMRmSmvZqxZBTTUFdeIdTyy-9E03e9B1wwNxZun-2WC1U1V9CixopnPdmGHIg%3D%3D?uid=198673594&filename=cat.jpg&disposition=attachment&hash=&limit=0&content_type=image%2Fjpeg&fsize=496464&hid=082a961816dfa432eb3224ea6fc20e78&media_type=image&tknv=v2&etag=1a900654f710d4c8a05a34af6337e085",
                imageResource.getOriginalUrl());
        assertEquals("https://downloader.disk.yandex.ru/preview/70502e09a92d68392c90a644b4e256341a99bcc90b9a850a4953dd2b657f8875/inf/H7xROOCyjZKkKYvjDR42V9mbFRAZvoLTSMhggzH3JjqcyDXOG24b8ehvUCe63gao-krgQ5o-_fDSFGDxaoTGCQ%3D%3D?uid=198673594&filename=cat.jpg&disposition=inline&hash=&limit=0&content_type=image%2Fjpeg&tknv=v2&size=L&crop=1",
                imageResource.getPreviewUrl());
        assertEquals("2018-04-06T12:50:01+00:00", imageResource.getCreated());
        assertEquals(496464, imageResource.getSize());
    }

    @Test
    public void clearResources() {
        String getSingleResourcesSuccess = getStringFromFile( "get_single_resource_success.json");
        MockResponse mockResponseGetSingleResourcesSuccess = new MockResponse().setResponseCode(200).setBody(getSingleResourcesSuccess);
        server.enqueue(mockResponseGetSingleResourcesSuccess);

        galleryModel.reloadImageResources("token")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe();

        galleryModel.clearResources();

        assertEquals(0, galleryModel.getImageResourcesCount());
        assertEquals(true, galleryModel.hasAnyResourcesToLoadMore());
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

    /**
     * Функция для получения текста из файла с
     * заданным путем.
     *
     * @param path путь к файлу
     * @return полученная строка
     */
    private String getStringFromFile(String path) {
        URL uri = this.getClass().getClassLoader().getResource(path);
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(uri.getPath()));
            return new String(encoded);
        } catch (IOException ex) {
            Timber.d(ex);
            return "";
        }
    }

}