package com.deniszaplatnikov.imagegallery.ui.gallery;

import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.deniszaplatnikov.imagegallery.utils.RxSchedulersOverrideRule;
import com.deniszaplatnikov.imagegallery.utils.authorizationManagement.AuthorizationManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class GalleryPresenterTest {

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule public RxSchedulersOverrideRule rxSchedulersOverrideRule = new RxSchedulersOverrideRule();

    @Mock private GalleryContract.View view;
    @Mock private GalleryContract.Model model;
    @Mock private AuthorizationManager authorizationManager;
    @Mock private GalleryContract.ImageViewCell imageViewCell;

    @InjectMocks private GalleryPresenter galleryPresenter;

    @Test
    public void firstViewLoadingAndLoginSetup() {

        when(authorizationManager.isLoggedIn()).thenReturn(false);
        when(model.reloadImageResources(any())).thenReturn(Observable.empty());

        galleryPresenter.attachView(view, true);

        InOrder inOrder = inOrder(authorizationManager, view);
        inOrder.verify(authorizationManager).isLoggedIn();
        inOrder.verify(view).setLoginView();
    }

    @Test
    public void reattachAndLoginSetup() {

        when(authorizationManager.isLoggedIn()).thenReturn(false);

        galleryPresenter.attachView(view, false);

        InOrder inOrder = inOrder(authorizationManager, view);
        inOrder.verify(authorizationManager).isLoggedIn();
        inOrder.verify(view).setLoginView();
    }

    @Test
    public void firstViewLoadingAndLogoutSetup() {

        when(authorizationManager.isLoggedIn()).thenReturn(true);
        when(model.reloadImageResources(any())).thenReturn(Observable.empty());

        galleryPresenter.attachView(view, true);

        InOrder inOrder = inOrder(authorizationManager, view);
        inOrder.verify(authorizationManager).isLoggedIn();
        inOrder.verify(view).setLogoutView();
    }

    @Test
    public void reattachAndLogoutSetup() {

        when(authorizationManager.isLoggedIn()).thenReturn(true);
        when(model.reloadImageResources(any())).thenReturn(Observable.empty());

        galleryPresenter.attachView(view, true);

        InOrder inOrder = inOrder(authorizationManager, view);
        inOrder.verify(authorizationManager).isLoggedIn();
        inOrder.verify(view).setLogoutView();
    }

    @Test
    public void firstLoadAndShowNonEmptyImagesList() {
        when(authorizationManager.isLoggedIn()).thenReturn(true);
        when(model.getImageResourcesCount()).thenReturn(0, 5);
        when(model.reloadImageResources(any())).thenReturn(Observable.just(5));

        galleryPresenter.attachView(view, true);

        InOrder inOrder = inOrder(authorizationManager, view, model);
        inOrder.verify(view).showGalleryView(false);
        inOrder.verify(view).showEmptyGalleryView(false);
        inOrder.verify(view).showProgressView(true);

        inOrder.verify(authorizationManager).getToken();
        inOrder.verify(model).reloadImageResources(any());
        inOrder.verify(view).reloadImages();

        inOrder.verify(view).showEmptyGalleryView(false);
        inOrder.verify(view).showGalleryView(true);
        inOrder.verify(view).showProgressView(false);
    }

    @Test
    public void firstLoadAndShowEmptyImagesList() {
        when(authorizationManager.isLoggedIn()).thenReturn(true);
        when(model.getImageResourcesCount()).thenReturn(0, 0);
        when(model.reloadImageResources(any())).thenReturn(Observable.just(0));

        galleryPresenter.attachView(view, true);

        InOrder inOrder = inOrder(authorizationManager, view, model);
        inOrder.verify(view).showGalleryView(false);
        inOrder.verify(view).showEmptyGalleryView(false);
        inOrder.verify(view).showProgressView(true);

        inOrder.verify(authorizationManager).getToken();
        inOrder.verify(model).reloadImageResources(any());
        inOrder.verify(view, never()).reloadImages();

        inOrder.verify(view).showEmptyGalleryView(true);
        inOrder.verify(view).showGalleryView(false);
        inOrder.verify(view).showProgressView(false);
    }

    @Test
    public void reattachFromNonEmptyState() {
        when(model.getImageResourcesCount()).thenReturn(1);

        galleryPresenter.attachView(view, false);

        InOrder inOrder = inOrder(view, model);
        inOrder.verify(view).showEmptyGalleryView(false);
        inOrder.verify(view).showGalleryView(true);

        inOrder.verify(model, never()).reloadImageResources(anyString());
        inOrder.verify(view).reloadImages();
    }

    @Test
    public void reattachFromEmptyState() {
        when(model.getImageResourcesCount()).thenReturn(0);

        galleryPresenter.attachView(view, false);

        InOrder inOrder = inOrder(view, model);
        inOrder.verify(view).showEmptyGalleryView(true);
        inOrder.verify(view).showGalleryView(false);

        inOrder.verify(model, never()).reloadImageResources(anyString());
        inOrder.verify(view).reloadImages();
    }

    @Test
    public void refreshGallerySuccess() {

        when(model.reloadImageResources(any())).thenReturn(Observable.just(1));

        galleryPresenter.attachView(view, false);
        galleryPresenter.onRefreshGallery();

        verify(view).stopRefreshing();
    }

    @Test
    public void refreshGalleryFail() {
        when(model.reloadImageResources(any())).thenReturn(Observable.error(new Exception()));

        galleryPresenter.attachView(view, false);
        galleryPresenter.onRefreshGallery();

        verify(view).showInternetConnectionError();
        verify(view).stopRefreshing();
    }

    @Test
    public void attachViewWithTokenWithAccessDenied() {
        String accessDeniedToken = "imagegallery://token#error=access_denied&error_description=nvm";
        galleryPresenter.attachViewWithToken(view, false, accessDeniedToken);

        verify(view).showAuthorizationAccessDeniedError();
    }

    @Test
    public void attachViewWithTokenWithUnauthorizedClient() {
        String unauthorizedClientToken = "imagegallery://token#error=unauthorized_client&error_description=nvm";
        galleryPresenter.attachViewWithToken(view, false, unauthorizedClientToken);

        verify(view).showUnauthorizedClientError();
    }

    @Test
    public void attachViewWithTokenWithUnknownTokenError() {
        String unknownErrorToken = "imagegallery://token#error=test&error_description=nvm";
        galleryPresenter.attachViewWithToken(view, false, unknownErrorToken);

        verify(view).showUnknownError();
    }

    @Test
    public void attachViewWithNormalToken() {
        String token = "testToken";
        String normalToken = "imagegallery://token#access_token=" + token;
        galleryPresenter.attachViewWithToken(view, false, normalToken);

        verify(authorizationManager).login(token);
    }

    @Test
    public void attachViewWithEmptyToken() {
        String token = "";
        String emptyToken = "imagegallery://token#access_token=" + token;
        galleryPresenter.attachViewWithToken(view, false, emptyToken);

        verify(authorizationManager, never()).login(token);
        verify(view).showCannotReceiveAuthToken();
    }

    @Test
    public void attachViewWithInvalidTokenData() {
        String invalidToken = "imagegallery://token#somethingStange";
        galleryPresenter.attachViewWithToken(view, false, invalidToken);

        verify(authorizationManager, never()).login(invalidToken);
        verify(view).showCannotReceiveAuthToken();
    }

    @Test
    public void endOfGallerySuccess() {

        when(model.loadMoreImageResources(any())).thenReturn(Observable.just(1));
        when(model.getImageResourcesCount()).thenReturn(1);
        when(view.getNetworkAvailable()).thenReturn(true);
        when(model.hasAnyResourcesToLoadMore()).thenReturn(true);

        galleryPresenter.attachView(view, false);
        galleryPresenter.onEndOfGallery();

        verify(view, times(2)).showEmptyGalleryView(false);
        verify(view, times(2)).showGalleryView(true);
    }

    @Test
    public void endOfGalleryFail() {
        when(model.loadMoreImageResources(any())).thenReturn(Observable.error(new Exception()));
        when(view.getNetworkAvailable()).thenReturn(true);
        when(model.hasAnyResourcesToLoadMore()).thenReturn(true);

        galleryPresenter.attachView(view, false);
        galleryPresenter.onEndOfGallery();

        verify(view).showInternetConnectionError();
    }

    @Test
    public void onBindImageViewCell() {
        String previewUrl = "previewUrl";
        ImageResource imageResource = new ImageResource("", "", previewUrl, "", 0);

        when(model.getImageResource(0)).thenReturn(imageResource);

        galleryPresenter.onBindImageViewCell(imageViewCell, 0);
        verify(imageViewCell).loadImage(previewUrl);
    }

    @Test
    public void geImageResourceCount() {
        int count = 100;
        when(model.getImageResourcesCount()).thenReturn(100);

        int presenterCount = galleryPresenter.getImageResourcesCount();
        assertEquals(count, presenterCount);
    }

    @Test
    public void imageClicked() {
        ImageResource imageResource = new ImageResource("", "", "", "", 0);
        when(model.getImageResource(0)).thenReturn(imageResource);

        galleryPresenter.attachView(view, false);
        galleryPresenter.imageClicked(0);
        verify(view).navigateToImageDetailsView(imageResource);
    }

    @Test
    public void onChangeAuthorizationStateAfterLogin() {
        when(authorizationManager.isLoggedIn()).thenReturn(true);

        galleryPresenter.attachView(view, false);
        galleryPresenter.onChangeAuthorizationState();
        verify(view).showAcceptLogoutDialog();
    }

    @Test
    public void onChangeAuthorizationStateAfterLogout() {
        when(authorizationManager.isLoggedIn()).thenReturn(false);

        galleryPresenter.attachView(view, false);
        galleryPresenter.onChangeAuthorizationState();
        verify(view).navigateToAuthorizationView();
    }

    @Test
    public void logoutAccepted() {
        when(model.reloadImageResources(any())).thenReturn(Observable.empty());

        galleryPresenter.attachView(view, false);
        galleryPresenter.logoutAccepted();

        verify(view, times(2)).setLoginView();
        verify(authorizationManager).logout();
        verify(model).clearResources();
    }

    @Test
    public void showIncorrectDataError() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(400, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showIncorrectDataError();
    }

    @Test
    public void showNotAuthorizedError() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(401, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showNotAuthorizedError();
    }

    @Test
    public void showAccessDeniedErrorWith402Code() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(402, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showAccessDeniedError();
    }

    @Test
    public void showAccessDeniedErrorWith403Code() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(403, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showAccessDeniedError();
    }

    @Test
    public void showCannotFindResourceError() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(404, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showCannotFindResourceError();
    }

    @Test
    public void showWrongResourceFormatError() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(406, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showWrongResourceFormatError();
    }

    @Test
    public void showTooManyRequestsError() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(429, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showTooManyRequestsError();
    }

    @Test
    public void showServiceUnavailableError() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(503, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showServiceUnavailableError();
    }

    @Test
    public void showUnknownError() {
        galleryPresenter.attachView(view, false);

        HttpException httpException = new HttpException(Response.error(499, ResponseBody.create(MediaType.parse(""), "")));
        galleryPresenter.showError(httpException);
        verify(view).showUnknownError();
    }


    @After
    public void tearDown() {
        galleryPresenter.detachView();
        galleryPresenter.stop();
    }
}