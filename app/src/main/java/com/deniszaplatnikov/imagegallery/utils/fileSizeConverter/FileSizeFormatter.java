package com.deniszaplatnikov.imagegallery.utils.fileSizeConverter;

import java.text.DecimalFormat;

public class FileSizeFormatter implements IFileSizeFormatter {

    /**
     * Форматирует данный размер в байтах в соответсвующий размер максимального объема.
     * Например, 1024 байт -> 1 Кб.
     * @param size размер для форматирования.
     * @return строка вида 1.00 Байт.
     */
    @Override
    public String formatFileSize(long size) {
        if (size < 0) {
            return "-";
        }
        String convertedSize;

        double kilo = size / 1024.0;
        double mega = ((size / 1024.0) / 1024.0);
        double gigo = (((size / 1024.0) / 1024.0) / 1024.0);
        double tera = ((((size / 1024.0) / 1024.0) / 1024.0) / 1024.0);

        DecimalFormat dec = new DecimalFormat("0.00");

        if (tera > 1) {
            convertedSize = dec.format(tera).concat(" ").concat(FileSizeCodes.TERABYTES.toString());
        } else if (gigo > 1) {
            convertedSize = dec.format(gigo).concat(" ").concat((FileSizeCodes.GIGABYTES).toString());
        } else if (mega > 1) {
            convertedSize = dec.format(mega).concat(" ").concat((FileSizeCodes.MEGABYTES).toString());
        } else if (kilo > 1) {
            convertedSize = dec.format(kilo).concat(" ").concat((FileSizeCodes.KILOBYTES).toString());
        } else {
            convertedSize = dec.format(size).concat(" ").concat((FileSizeCodes.BYTES).toString());
        }

        return convertedSize;
    }

}
