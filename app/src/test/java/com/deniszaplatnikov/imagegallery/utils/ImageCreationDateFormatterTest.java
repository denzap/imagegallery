package com.deniszaplatnikov.imagegallery.utils;

import org.junit.After;
import org.junit.Test;

import java.util.Locale;
import java.util.TimeZone;

import static org.junit.Assert.*;

public class ImageCreationDateFormatterTest {

    private TimeZone originalTimeZone = TimeZone.getDefault();
    private Locale originalLocale = Locale.getDefault();

    @Test
    public void validRussianMoscowFormattedDateFormatting() {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Moscow"));
        Locale.setDefault(new Locale("ru", "RU"));

        String created = "2018-04-06T12:50:01+00:00";
        assertEquals("06 апреля 2018, 15:50", ImageCreationDateFormatter.formatDate(created));
    }

    @Test
    public void validUtcEnglishFormattedDateFormatting() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Locale.setDefault(new Locale("en", "EN"));

        String created = "2018-04-06T12:50:01+00:00";
        assertEquals("06 April 2018, 12:50", ImageCreationDateFormatter.formatDate(created));
    }

    @Test
    public void invalidFormattedDateFormatting() {
        String created = "2018-04T12:50:01+00:00";
        assertEquals("-", ImageCreationDateFormatter.formatDate(created));
    }

    @Test
    public void nullDateFormatting() {
        assertEquals("-", ImageCreationDateFormatter.formatDate(null));
    }

    @After
    public void tearDown() {
        TimeZone.setDefault(originalTimeZone);
        Locale.setDefault(originalLocale);
    }
}