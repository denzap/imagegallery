package com.deniszaplatnikov.imagegallery.di.test;

import com.deniszaplatnikov.imagegallery.data.YandexDiskService;

import dagger.Component;

@Component(modules = {TestNetworkModule.class})
public interface TestNetworkComponent {

    YandexDiskService getYandexDiskService();

}
