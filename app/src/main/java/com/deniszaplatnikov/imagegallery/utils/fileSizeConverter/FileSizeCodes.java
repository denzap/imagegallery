package com.deniszaplatnikov.imagegallery.utils.fileSizeConverter;

import com.deniszaplatnikov.imagegallery.R;
import com.deniszaplatnikov.imagegallery.ImageGalleryApplication;

/**
 *  Enum для различных размеров файлов от Байт до ТБ
 */
enum FileSizeCodes {

    BYTES(R.string.byte_short),
    KILOBYTES(R.string.kilobyte_short),
    MEGABYTES(R.string.megabyte_short),
    GIGABYTES(R.string.gigabyte_short),
    TERABYTES(R.string.terabyte_short);

    private int resourceId;

    FileSizeCodes(int id) {
        resourceId = id;
    }

    @Override
    public String toString() {
        // получим локализованное сокращение размера
        return ImageGalleryApplication.getContext().getString(resourceId);
    }
}
