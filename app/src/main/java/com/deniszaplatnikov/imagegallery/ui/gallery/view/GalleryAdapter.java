package com.deniszaplatnikov.imagegallery.ui.gallery.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.deniszaplatnikov.imagegallery.R;
import com.deniszaplatnikov.imagegallery.ui.gallery.GalleryContract;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryImageViewHolder> {

    private final GalleryContract.Presenter presenter;

    GalleryAdapter(GalleryContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public GalleryImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GalleryImageViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_image_cell, parent, false), presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryImageViewHolder holder, int position) {
        presenter.onBindImageViewCell(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getImageResourcesCount();
    }
}
