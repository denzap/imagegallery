package com.deniszaplatnikov.imagegallery.ui.imageDetails;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.deniszaplatnikov.imagegallery.ImageGalleryApplication;
import com.deniszaplatnikov.imagegallery.R;
import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.squareup.picasso.Callback;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import timber.log.Timber;

public class ImageDetailsActivity extends AppCompatActivity implements ImageDetailsContract.View {

    public final static String EXTRA_IMAGE_DETAILS = "imageDetails";

    @Inject ImageDetailsContract.Presenter presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.toolbar_title) TextView toolbarTitle;

    @BindView(R.id.right_item_image) ImageView toolbarRightItemImageView;

    @BindView(R.id.right_item_text) TextView toolbarRightItemTextView;

    @BindView(R.id.progress_bar_layout) LinearLayout progressBarLayout;

    @BindView(R.id.reload_image_layout) LinearLayout reloadImageViewLayout;

    @BindView(R.id.reload_image) ImageView reloadImageView;

    @BindView(R.id.reload_image_error_text) TextView reloadImageErrorTextView;

    @BindView(R.id.image_details_scroll_view) ScrollView imageDetailsScrollView;

    @BindView(R.id.image) ImageView image;

    @BindView(R.id.text_image_made_time) TextView imageMadeTimeTextView;

    @BindView(R.id.text_image_name) TextView imageNameTextView;

    @BindView(R.id.text_image_size) TextView imageSizeTextView;

    private Animation rotation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_details);

        ButterKnife.bind(this);
        setUpToolbar();

        // восстановление состояния presenter
        presenter = (ImageDetailsContract.Presenter) getLastCustomNonConfigurationInstance();
        boolean isNewPresenter = presenter == null;
        if (presenter == null) {
            AndroidInjection.inject(this);
        }

        presenter.attachView(this, isNewPresenter);

        // получение данных об изображении
        Bundle extras = getIntent().getExtras();
        if (savedInstanceState == null && extras != null) {
            ImageResource imageResource = extras.getParcelable(EXTRA_IMAGE_DETAILS);
            if (imageResource != null) {
                presenter.setImageDetails(imageResource);
            }
        }
    }

    private void setUpToolbar() {
        toolbarTitle.setText(R.string.view);

        // настройка кнопки сохранения изображения
        toolbarRightItemTextView.setVisibility(View.GONE);
        toolbarRightItemImageView.setImageResource(R.drawable.ic_file_download_black);

        // установка заголовка toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        // настройка перехода на предыдущий экран
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    @OnClick(R.id.right_item)
    protected void onSaveImageClicked() {
        presenter.onSaveImageClicked();
    }

    @OnClick(R.id.reload_image)
    protected void onReloadImageClicked() {
        presenter.onReloadImageClicked();
    }

    @Override
    public void loadImage(String url) {
        ImageGalleryApplication.getPicasso().load(url).into(image, new Callback() {
            @Override
            public void onSuccess() {
                presenter.imageLoaded(true);
            }

            @Override
            public void onError(Exception e) {
                Timber.d(e);
                presenter.imageLoaded(false);
            }
        });
    }

    @Override
    public void setImageMadeTime(String time) {
        imageMadeTimeTextView.setText(String.format("%s: %s", getResources().getString(R.string.image_created), time));
    }

    @Override
    public void setImageName(String name) {
        imageNameTextView.setText(String.format("%s: %s", getResources().getString(R.string.image_name), name));
    }

    @Override
    public void setImageSize(String size) {
        imageSizeTextView.setText(String.format("%s: %s", getResources().getString(R.string.image_size), size));
    }

    @Override
    public void showProgressView(boolean show) {
        progressBarLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void startReloadImageAnimation() {
        rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        reloadImageView.startAnimation(rotation);
        reloadImageView.setEnabled(false);
        reloadImageErrorTextView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void stopReloadImageAnimation() {
        // производим 'докручивание' анимации,
        // иначе вьюха резко вернется в начальное положение без окончания анимации
        if (rotation != null) {
            rotation.setRepeatCount(0);
        }
        reloadImageView.setEnabled(true);
        reloadImageErrorTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showImageDetailsView(boolean show) {
        imageDetailsScrollView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showImage(boolean show) {
        image.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showLoadImageError(boolean show) {
        reloadImageViewLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void saveImage(String url) {
        // делегируем сохранение изображения браузеру пользователя
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        if (!isChangingConfigurations()) {
            presenter.stop();
        }
        super.onDestroy();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenter;
    }
}
