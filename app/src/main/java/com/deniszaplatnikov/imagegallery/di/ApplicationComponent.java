package com.deniszaplatnikov.imagegallery.di;

import android.content.Context;

import com.deniszaplatnikov.imagegallery.ImageGalleryApplication;
import com.squareup.picasso.Picasso;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@AppScope
@Component(modules = {ApplicationModule.class, ApplicationModule.Declarations.class, ActivityBindingModule.class, AndroidSupportInjectionModule.class})
public interface ApplicationComponent extends AndroidInjector<ImageGalleryApplication> {

    /**
     * Дает синтаксический сахар. Теперь можно с помощью
     * DaggerApplicationComponent.builder().application(this).build().inject(this);
     * никогда не создавать модули явно или указывать в какой модуль мы передаем application.
     * Application уже будет содержаться в нашем графе зависимостей.
     */
    @Component.Builder
    interface Builder {

        @BindsInstance
        ApplicationComponent.Builder application(ImageGalleryApplication application);

        ApplicationComponent build();
    }

    Context getContext();

    Picasso getPicasso();
}
