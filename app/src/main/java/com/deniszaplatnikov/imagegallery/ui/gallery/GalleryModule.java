package com.deniszaplatnikov.imagegallery.ui.gallery;

import com.deniszaplatnikov.imagegallery.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class GalleryModule {

    @Binds
    @ActivityScoped
    public abstract GalleryContract.Model bindGalleryModel(GalleryModel galleryModel);

    @Binds
    @ActivityScoped
    public abstract GalleryContract.Presenter bindGalleryPresenter(GalleryPresenter presenter);
}
