package com.deniszaplatnikov.imagegallery.ui.imageDetails;

import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.deniszaplatnikov.imagegallery.utils.fileSizeConverter.IFileSizeFormatter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.*;

public class ImageDetailsPresenterTest {

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock private ImageDetailsContract.View view;

    @Mock private IFileSizeFormatter fileSizeFormatter;

    @InjectMocks private ImageDetailsPresenter imageDetailsPresenter;

    private String imageName = "name";
    private String originalUrl = "originalUrl";
    private String shortUrl = "shortUrl";
    private String created = "2018-04-06T12:50:01+00:00";
    private ImageResource normalImageResource = new ImageResource(imageName, originalUrl, shortUrl, created, 1024);
    private ImageResource nullableImageResource = new ImageResource(null, null, null, null, -1);

    @Before
    public void setUp() {
        imageDetailsPresenter.attachView(view, true);
    }

    @Test
    public void firstViewLoading() {
        verify(view).showProgressView(true);
        verify(view).showImageDetailsView(false);
    }

    @Test
    public void imageWasLoadedSuccessfully() {
        imageDetailsPresenter.imageLoaded(true);

        verify(view).showProgressView(false);
        verify(view).showImageDetailsView(true);
        verify(view).showImage(true);
        verify(view).showLoadImageError(false);
        verify(view).stopReloadImageAnimation();
    }

    @Test
    public void imageWasLoadedFail() {
        imageDetailsPresenter.imageLoaded(false);

        verify(view).showProgressView(false);
        verify(view).showImageDetailsView(true);
        verify(view).showImage(false);
        verify(view).showLoadImageError(true);
        verify(view).stopReloadImageAnimation();
    }

    @Test
    public void normalImageDetailsWereSet() {
        imageDetailsPresenter.setImageDetails(normalImageResource);

        verify(view).setImageName(imageName);
        verify(view).setImageMadeTime(anyString());
        verify(view).setImageSize(any());
    }

    @Test
    public void nullableImageDetailsWereSet() {
        imageDetailsPresenter.setImageDetails(nullableImageResource);

        verify(view).setImageName(null);
        verify(view).setImageMadeTime("-");
        verify(view).setImageSize(any());
    }

    @Test
    public void reloadImage() {
        InOrder inOrder = inOrder(view);
        imageDetailsPresenter.setImageDetails(normalImageResource);

        inOrder.verify(view).loadImage(originalUrl);

        imageDetailsPresenter.onReloadImageClicked();
        inOrder.verify(view).startReloadImageAnimation();
        inOrder.verify(view).loadImage(originalUrl);

        imageDetailsPresenter.imageLoaded(anyBoolean());
        inOrder.verify(view).stopReloadImageAnimation();
    }

    @Test
    public void showAlreadyExistedResourceWhenViewRecreated() {
        imageDetailsPresenter.setImageDetails(normalImageResource);

        reset(view);
        imageDetailsPresenter.detachView();
        imageDetailsPresenter.attachView(view, false);

        verify(view).loadImage(originalUrl);
        verify(view).setImageName(imageName);
        verify(view).setImageMadeTime(anyString());
        verify(view).setImageSize(any());
    }

    @Test
    public void onSaveImageClicked() {
        imageDetailsPresenter.setImageDetails(normalImageResource);

        imageDetailsPresenter.onSaveImageClicked();
        verify(view).saveImage(originalUrl);
    }

    @After
    public void setDown() {
        imageDetailsPresenter.detachView();
        imageDetailsPresenter.stop();
    }
}
