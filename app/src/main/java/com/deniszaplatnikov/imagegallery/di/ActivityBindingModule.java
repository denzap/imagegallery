package com.deniszaplatnikov.imagegallery.di;

import com.deniszaplatnikov.imagegallery.ui.gallery.GalleryModule;
import com.deniszaplatnikov.imagegallery.ui.gallery.view.GalleryActivity;
import com.deniszaplatnikov.imagegallery.ui.imageDetails.ImageDetailsModule;
import com.deniszaplatnikov.imagegallery.ui.imageDetails.ImageDetailsActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = GalleryModule.class)
    abstract GalleryActivity galleryActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ImageDetailsModule.class)
    abstract ImageDetailsActivity imageDetailsActivity();

}
