package com.deniszaplatnikov.imagegallery.utils.fileSizeConverter;

/**
 * Форматирует данный размер в байтах в соответсвующий размер максимального объема.
 */
public interface IFileSizeFormatter {
    String formatFileSize(long size);
}
