package com.deniszaplatnikov.imagegallery.ui.authorization;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deniszaplatnikov.imagegallery.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthorizationActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.toolbar_title) TextView toolbarTitle;

    @BindView(R.id.right_item) RelativeLayout toolbarRightItemLayout;

    @BindView(R.id.progress_bar_layout) LinearLayout progressBarLayout;

    @BindView(R.id.web_view_authorization) WebView authorizationWebView;

    private final static String clientId = "b7cb76e886eb4920b2c200d00ce51d31";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);

        ButterKnife.bind(this);

        setUpToolbar();
        setUpAuthorizationView();
    }

    private void setUpToolbar() {
        toolbarRightItemLayout.setVisibility(View.GONE);
        toolbarTitle.setText(R.string.authorization);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setUpAuthorizationView() {
        progressBarLayout.setVisibility(View.VISIBLE);
        authorizationWebView.setVisibility(View.GONE);
        authorizationWebView.getSettings().setJavaScriptEnabled(true);

        // установка кастомного веб клиента, чтобы осуществлять работу с браузером внутри приложения
        authorizationWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Uri uri = Uri.parse(url);
                if (uri.getScheme().equals("imagegallery")) {
                    // пользователь совершил действие с авторизацией
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(uri);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    // загрузка обычного Url
                    view.loadUrl(url);
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBarLayout.setVisibility(View.GONE);
                authorizationWebView.setVisibility(View.VISIBLE);
            }
        });

        // открытие страницы авторизации и предоставление прав чтения диска пользователя
        authorizationWebView.loadUrl(String.format("https://oauth.yandex.ru/authorize?response_type=token&client_id=%s&force_confirm=1", clientId));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
