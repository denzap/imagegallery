package com.deniszaplatnikov.imagegallery;

import android.content.Context;

import com.deniszaplatnikov.imagegallery.di.ApplicationComponent;
import com.deniszaplatnikov.imagegallery.di.DaggerApplicationComponent;
import com.deniszaplatnikov.imagegallery.utils.NotLoggingTree;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.picasso.Picasso;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.reactivex.plugins.RxJavaPlugins;
import timber.log.Timber;

public class ImageGalleryApplication extends DaggerApplication {

    private static ApplicationComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        else {
            Timber.plant(new NotLoggingTree());
        }

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);

        // Отключить вызов exceptions после отписки от observable
        RxJavaPlugins.setErrorHandler(throwable -> Timber.d(throwable.getMessage()));

    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        appComponent = DaggerApplicationComponent.builder().application(this).build();
        return appComponent;
    }

    public static Context getContext() {
        return appComponent.getContext();
    }

    public static Picasso getPicasso() {
        return appComponent.getPicasso();
    }

}
