package com.deniszaplatnikov.imagegallery.di.test;

import com.deniszaplatnikov.imagegallery.data.YandexDiskService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class TestNetworkModule {

    private String baseUrl;

    public TestNetworkModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    public YandexDiskService provideYandexDiskService(Retrofit retrofit) {
        return retrofit.create(YandexDiskService.class);
    }

    @Provides
    public Retrofit provideRetrofit(OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxJavaCallAdapterFactory) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .client(okHttpClient)
                .build();
    }

    @Provides
    public GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    public RxJava2CallAdapterFactory provideRxJavaCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    public OkHttpClient provideRetrofitOkHttpClient() {
        return new OkHttpClient.Builder().build();
    }
}
