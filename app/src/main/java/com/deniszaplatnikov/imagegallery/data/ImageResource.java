package com.deniszaplatnikov.imagegallery.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Сущность ресурса изображения.
 * Предоставляет информацию о названии изображения, его полноразмерный и preview url.
 */
public class ImageResource implements Parcelable {

    @SerializedName("name") private String name;
    @SerializedName("file") private String originalUrl;
    @SerializedName("preview") private String previewUrl;
    @SerializedName("created") private String created;
    @SerializedName("size") private long size;

    public ImageResource(String name, String originalUrl, String previewUrl, String created, long size) {
        this.name = name;
        this.originalUrl = originalUrl;
        this.previewUrl = previewUrl;
        this.created = created;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(originalUrl);
        parcel.writeString(previewUrl);
        parcel.writeString(created);
        parcel.writeLong(size);
    }

    public static final Parcelable.Creator<ImageResource> CREATOR = new Parcelable.Creator<ImageResource>() {
        public ImageResource createFromParcel(Parcel parcel) {
            return new ImageResource(parcel);
        }

        public ImageResource[] newArray(int size) {
            return new ImageResource[size];
        }
    };

    private ImageResource(Parcel parcel) {
        name = parcel.readString();
        originalUrl = parcel.readString();
        previewUrl = parcel.readString();
        created = parcel.readString();
        size = parcel.readLong();
    }
}
