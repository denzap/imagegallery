package com.deniszaplatnikov.imagegallery.ui.gallery.view;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.deniszaplatnikov.imagegallery.ui.authorization.AuthorizationActivity;
import com.deniszaplatnikov.imagegallery.R;
import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.deniszaplatnikov.imagegallery.ui.gallery.GalleryContract;
import com.deniszaplatnikov.imagegallery.ui.imageDetails.ImageDetailsActivity;
import com.deniszaplatnikov.imagegallery.utils.NetworkInfoUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

public class GalleryActivity extends AppCompatActivity implements GalleryContract.View {

    @Inject GalleryContract.Presenter presenter;

    @BindView(R.id.toolbar_title) TextView toolbarTitle;

    @BindView(R.id.right_item) RelativeLayout toolbarRightItemLayout;

    @BindView(R.id.right_item_image) ImageView toolbarRightItemImageView;

    @BindView(R.id.right_item_text) TextView toolbarRightItemTextView;

    @BindView(R.id.progress_bar_layout) LinearLayout progressBarLayout;

    @BindView(R.id.empty_gallery_layout) LinearLayout emptyGalleryLayout;

    @BindView(R.id.recycler_image_gallery) RecyclerView imageGalleryRecyclerView;

    @BindView(R.id.refresh_gallery_swipe_container) SwipeRefreshLayout refreshGallerySwipeContainer;

    private GalleryAdapter galleryAdapter;
    private GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        ButterKnife.bind(this);

        // восстановление состояния presenter
        presenter = (GalleryContract.Presenter) getLastCustomNonConfigurationInstance();
        boolean isNewPresenter = presenter == null;
        if (presenter == null) {
            AndroidInjection.inject(this);
        }

        setUpProgressBarLayout();
        setUpRecyclerImageGalleryView();
        setUpRefreshGallerySwipeContainer();
        setUpEndlessScroll();

        if (savedInstanceState == null
                && getIntent() != null
                && getIntent().getData() != null) {
            // получение данных после авторизации
            Uri data = getIntent().getData();
            presenter.attachViewWithToken(this, isNewPresenter, data.getFragment());
        } else {
            presenter.attachView(this, isNewPresenter);
        }
    }

    private void setUpProgressBarLayout() {
        progressBarLayout.setBackgroundResource(R.color.lightGray);
    }

    /**
     * Установка списка изображений, расположенных в сетке на 2 столбца.
     */
    private void setUpRecyclerImageGalleryView() {
        galleryAdapter = new GalleryAdapter(presenter);
        int numberOfColumns = 2;
        gridLayoutManager = new GridLayoutManager(this, numberOfColumns, GridLayoutManager.VERTICAL, false);
        imageGalleryRecyclerView.setLayoutManager(gridLayoutManager);
        imageGalleryRecyclerView.setAdapter(galleryAdapter);
    }

    private void setUpRefreshGallerySwipeContainer() {
        refreshGallerySwipeContainer.setOnRefreshListener(() -> presenter.onRefreshGallery());
        refreshGallerySwipeContainer.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    /**
     * Установка бесконечного скрола.
     * Подгрузка новых элементов начинается, когда до конца списка остается
     * visibleThreshold элементов
     */
    private void setUpEndlessScroll() {
        imageGalleryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private int visibleThreshold = 5;
            private int lastVisibleItem, totalItemCount;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = gridLayoutManager.getItemCount();
                lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                if (totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    presenter.onEndOfGallery();
                }
            }
        });
    }

    @OnClick(R.id.right_item)
    protected void onChangeAuthorizationStateClicked() {
        presenter.onChangeAuthorizationState();
    }

    @OnClick(R.id.btn_reload)
    protected void onReloadClicked() {
        presenter.onRefreshGallery();
    }

    @Override
    public void setLoginView() {
        toolbarTitle.setText(R.string.general_feed);
        toolbarRightItemTextView.setText(R.string.login);
        toolbarRightItemImageView.setImageResource(R.drawable.ic_account_circle_black);
    }

    @Override
    public void setLogoutView() {
        toolbarTitle.setText(R.string.my_feed);
        toolbarRightItemTextView.setText(R.string.logout);
        toolbarRightItemImageView.setImageResource(R.drawable.ic_yandex_disk);
    }

    @Override
    public void reloadImages() {
        galleryAdapter.notifyDataSetChanged();
    }

    @Override
    public void navigateToImageDetailsView(@NonNull ImageResource imageResource) {
        Intent intent = new Intent(this, ImageDetailsActivity.class);
        intent.putExtra(ImageDetailsActivity.EXTRA_IMAGE_DETAILS, imageResource);
        startActivity(intent);
    }

    @Override
    public void navigateToAuthorizationView() {
        Intent intent = new Intent(this, AuthorizationActivity.class);
        startActivity(intent);
    }

    @Override
    public void showAcceptLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.logout_info_message).setTitle(R.string.attention);
        builder.setPositiveButton(R.string.yes, (dialog, id) -> presenter.logoutAccepted());
        builder.setNegativeButton(R.string.no, null);
        builder.create().show();
    }

    @Override
    public void showUserLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.user_logout_info_message).setTitle(R.string.attention);
        builder.setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    @Override
    public void showProgressView(boolean show) {
        progressBarLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showEmptyGalleryView(boolean show) {
        emptyGalleryLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showGalleryView(boolean show) {
        refreshGallerySwipeContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void stopRefreshing() {
        refreshGallerySwipeContainer.setRefreshing(false);
    }

    @Override
    public void showAuthorizationAccessDeniedError() {
        showErrorMessage(getResources().getString(R.string.error_authorization_access_denied));
    }

    @Override
    public void showUnauthorizedClientError() {
        showErrorMessage(getResources().getString(R.string.error_unauthorized_client));
    }

    @Override
    public void showCannotReceiveAuthToken() {
        showErrorMessage(getResources().getString(R.string.error_receive_token));
    }

    @Override
    public void showIncorrectDataError() {
        showErrorMessage(getResources().getString(R.string.error_incorrect_data));
    }

    @Override
    public void showNotAuthorizedError() {
        showErrorMessage(getResources().getString(R.string.error_not_authorized));
    }

    @Override
    public void showAccessDeniedError() {
        showErrorMessage(getResources().getString(R.string.error_access_denied));
    }

    @Override
    public void showCannotFindResourceError() {
        showErrorMessage(getResources().getString(R.string.error_cannot_find_resource));
    }

    @Override
    public void showWrongResourceFormatError() {
        showErrorMessage(getResources().getString(R.string.error_wrong_resource_format));
    }

    @Override
    public void showTooManyRequestsError() {
        showErrorMessage(getResources().getString(R.string.error_too_many_requests));
    }

    @Override
    public void showServiceUnavailableError() {
        showErrorMessage(getResources().getString(R.string.error_service_unavailable));
    }

    @Override
    public void showInternetConnectionError() {
        showErrorMessage(getResources().getString(R.string.error_no_internet_connection));
    }

    @Override
    public void showUnknownError() {
        showErrorMessage(getResources().getString(R.string.error_unknown));
    }

    @Override
    public boolean getNetworkAvailable() {
        return NetworkInfoUtils.isNetworkAvailable(this);
    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        if (!isChangingConfigurations()) {
            presenter.stop();
        }
        super.onDestroy();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenter;
    }
}
