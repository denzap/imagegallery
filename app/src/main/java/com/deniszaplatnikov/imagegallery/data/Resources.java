package com.deniszaplatnikov.imagegallery.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Ответ сервера на GET запрос resources/files.
 * Содержит в себе информацию об изображениях на диске пользователя,
 * ограничение на количество получаемых изображений
 * и сдвиг относительно первого изображения.
 */
public class Resources {

    @SerializedName("items") private List<ImageResource> items;
    @SerializedName("limit") private int limit;
    @SerializedName("offset") private int offset;

    public Resources(List<ImageResource> items, int limit, int offset) {
        this.items = items;
        this.limit = limit;
        this.offset = offset;
    }

    public List<ImageResource> getItems() {
        return items;
    }

    public void setItems(List<ImageResource> items) {
        this.items = items;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
