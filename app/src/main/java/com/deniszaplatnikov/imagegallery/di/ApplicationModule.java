package com.deniszaplatnikov.imagegallery.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.deniszaplatnikov.imagegallery.ImageGalleryApplication;
import com.deniszaplatnikov.imagegallery.data.YandexDiskService;
import com.deniszaplatnikov.imagegallery.di.qualifier.QPicassoCache;
import com.deniszaplatnikov.imagegallery.di.qualifier.QPicassoOkHttpClient;
import com.deniszaplatnikov.imagegallery.di.qualifier.QRetrofitCache;
import com.deniszaplatnikov.imagegallery.di.qualifier.QRetrofitOkHttpClient;
import com.deniszaplatnikov.imagegallery.utils.authorizationManagement.AuthorizationManager;
import com.deniszaplatnikov.imagegallery.utils.authorizationManagement.ContentTokenInterceptor;
import com.deniszaplatnikov.imagegallery.utils.authorizationManagement.YandexDiskAuthorizationManager;
import com.deniszaplatnikov.imagegallery.utils.fileSizeConverter.FileSizeFormatter;
import com.deniszaplatnikov.imagegallery.utils.fileSizeConverter.IFileSizeFormatter;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module
public class ApplicationModule {

    @Module
    interface Declarations {
        @AppScope
        @Binds
        Context bindContext(ImageGalleryApplication application);
    }

    private final static String IMAGE_GALLERY_SHARED_PREFERENCES = "com.deniszaplatnikov.imagegallery.prefs";

    @AppScope
    @Provides
    public IFileSizeFormatter bindFileSizeFormatter() {
        return new FileSizeFormatter();
    }

    @AppScope
    @Provides
    public AuthorizationManager bindAuthorizationManager(SharedPreferences sharedPreferences, ContentTokenInterceptor contentTokenInterceptor) {
        return new YandexDiskAuthorizationManager(sharedPreferences, contentTokenInterceptor);
    }

    @AppScope
    @Provides
    public ContentTokenInterceptor bindContentTokenInterceptor() {
        return new ContentTokenInterceptor();
    }

    @AppScope
    @Provides
    public YandexDiskService provideYandexDiskService(Retrofit retrofit) {
        return retrofit.create(YandexDiskService.class);
    }

    @AppScope
    @Provides
    public Retrofit provideRetrofit(@QRetrofitOkHttpClient OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxJavaCallAdapterFactory) {
        return new Retrofit.Builder()
                .baseUrl("https://cloud-api.yandex.net/v1/disk/")
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .client(okHttpClient)
                .build();
    }

    @AppScope
    @Provides
    public Picasso providePicasso(Context context, @QPicassoOkHttpClient OkHttpClient picassoClient) {
        Picasso picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(picassoClient))
                .build();
        Picasso.setSingletonInstance(picasso);
        return picasso;
    }

    @AppScope
    @Provides
    public GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @AppScope
    @Provides
    public RxJava2CallAdapterFactory provideRxJavaCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @AppScope
    @Provides
    @QRetrofitOkHttpClient
    public OkHttpClient provideRetrofitOkHttpClient(@QRetrofitCache Cache cache, HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @AppScope
    @Provides
    @QPicassoOkHttpClient
    public OkHttpClient providePicassoOkHttpClient(@QPicassoCache Cache cache, ContentTokenInterceptor contentTokenInterceptor) {
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(contentTokenInterceptor)
                .build();
    }

    @AppScope
    @Provides
    @QRetrofitCache
    public Cache provideRetrofitCache(Context context) {
        File httpCacheDirectory = new File(context.getCacheDir(), "retrofit_cache");
        return new Cache(httpCacheDirectory, 15 * 1024 * 1024);
    }

    @AppScope
    @Provides
    @QPicassoCache
    public Cache providePicassoCache(Context context) {
        File httpCacheDirectory = new File(context.getCacheDir(), "picasso_cache");
        return new Cache(httpCacheDirectory, 15 * 1024 * 1024);
    }

    @AppScope
    @Provides
    public HttpLoggingInterceptor provideLoggingInterceptor() {
        return new HttpLoggingInterceptor(message -> Timber.tag("okHttp").d(message)).setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @AppScope
    @Provides
    public SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(IMAGE_GALLERY_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }
}
