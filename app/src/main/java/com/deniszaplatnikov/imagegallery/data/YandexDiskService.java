package com.deniszaplatnikov.imagegallery.data;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface YandexDiskService {

    @GET("resources/files")
    Observable<Resources> getResources(@Header("Authorization") String token,
                                                 @Query("media_type") String mediaType,
                                                 @Query("preview_size") String previewSize,
                                                 @Query("preview_crop") boolean previewCrop,
                                                 @Query("limit") int limit,
                                                 @Query("offset") int offset);
}
