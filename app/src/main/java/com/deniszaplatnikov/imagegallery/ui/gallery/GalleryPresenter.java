package com.deniszaplatnikov.imagegallery.ui.gallery;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.deniszaplatnikov.imagegallery.utils.authorizationManagement.AuthorizationManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class GalleryPresenter implements GalleryContract.Presenter {

    @Nullable private GalleryContract.View view;
    private final GalleryContract.Model model;
    private final AuthorizationManager authorizationManager;

    private CompositeDisposable disposables = new CompositeDisposable();
    private boolean isReloading = false;
    private boolean isLoadedMore = false;

    @Inject
    GalleryPresenter(GalleryContract.Model model, AuthorizationManager authorizationManager) {
        this.model = model;
        this.authorizationManager = authorizationManager;
    }

    @Override
    public void attachView(GalleryContract.View view, boolean isNew) {
        this.view = view;

        setUpInitialGalleryView(isNew);
    }

    @Override
    public void attachViewWithToken(@NonNull GalleryContract.View view, boolean isNew, @NonNull String tokenData) {
        this.view = view;

        // Получение информации об ошибке авторизации
        Pattern errorPattern = Pattern.compile("error=(.*?)&error_description=(.*?)(&|$)");
        Matcher errorMatcher = errorPattern.matcher(tokenData);
        if (errorMatcher.find()) {
            String error = errorMatcher.group(1);
            switch (error) {
                case "access_denied":
                    this.view.showAuthorizationAccessDeniedError();
                    break;
                case "unauthorized_client":
                    this.view.showUnauthorizedClientError();
                    break;
                default:
                    this.view.showUnknownError();
                    break;
            }
        } else {
            // получение авторизационного токена
            Pattern successPattern = Pattern.compile("access_token=(.*?)(&|$)");
            Matcher successMatcher = successPattern.matcher(tokenData);
            if (successMatcher.find()) {
                String token = successMatcher.group(1);
                if (!token.isEmpty()) {
                    authorizationManager.login(token);
                } else {
                    this.view.showCannotReceiveAuthToken();
                }
            } else {
                this.view.showCannotReceiveAuthToken();
            }
        }
        setUpInitialGalleryView(isNew);
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public void stop() {
        disposables.dispose();
    }

    @Override
    public void onRefreshGallery() {
        reloadImageResources();
    }

    @Override
    public void onEndOfGallery() {
        // если нет сети, то не пытаться загружать новые данные
        if (view != null && !view.getNetworkAvailable()) {
            return;
        }

        // не загружать больше изображений пока идет другая загрузка
        // или пока больше не останется ресурсов для загрузки
        if (isReloading || isLoadedMore || !model.hasAnyResourcesToLoadMore()) {
            return;
        }

        isLoadedMore = true;

        disposables.add(model.loadMoreImageResources(authorizationManager.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (Integer deliveredCount) -> {
                            // обновление вьюхи, если были получены > 0 ресурсов
                            if (deliveredCount > 0 && view != null) {
                                view.reloadImages();
                            }
                            updateViewBasedOnResourcesCount(model.getImageResourcesCount());
                            isLoadedMore = false;
                        },
                        (Throwable error) -> {
                            updateViewBasedOnResourcesCount(model.getImageResourcesCount());
                            showError(error);
                            isLoadedMore = false;
                            // выход из аккаунта, если пользователь стал неавторизован
                            if (checkIfUserHasLoggedOut(error)) {
                                logout();
                                if (view != null) {
                                    view.showUserLogoutDialog();
                                }
                            }
                        }
                ));
    }

    @Override
    public void onBindImageViewCell(@NonNull GalleryContract.ImageViewCell imageViewCell, int position) {
        ImageResource imageResource = model.getImageResource(position);
        if (imageResource != null) {
            imageViewCell.loadImage(imageResource.getPreviewUrl());
        }
    }

    @Override
    public int getImageResourcesCount() {
        return model.getImageResourcesCount();
    }

    @Override
    public void imageClicked(int position) {
        if (view != null) {
            ImageResource imageResource = model.getImageResource(position);
            if (imageResource != null) {
                view.navigateToImageDetailsView(imageResource);
            }
        }
    }

    @Override
    public void onChangeAuthorizationState() {
        if (view != null) {
            if (!authorizationManager.isLoggedIn()) {
                view.navigateToAuthorizationView();
            } else {
                view.showAcceptLogoutDialog();
            }
        }
    }

    @Override
    public void logoutAccepted() {
        logout();
    }

    private void logout() {
        authorizationManager.logout();
        model.clearResources();
        if (view != null) {
            view.reloadImages();
            view.setLoginView();
        }
        reloadImageResources();
    }

    private void setUpInitialGalleryView(boolean isNew) {
        setUpAuthorizationView();
        if (isNew) {
            reloadImageResources();
        } else {
            updateViewBasedOnResourcesCount(model.getImageResourcesCount());
            if (view != null) {
                view.reloadImages();
            }
        }
    }

    private void reloadImageResources() {
        // не перезагружать, если уже идет перезагрузка
        if (isReloading) {
            return;
        }

        // остановка загрузки другой части списка,
        // т.к. список будет полностью обновлен
        if (isLoadedMore) {
            disposables.clear();
        }

        startReloading();
        disposables.add(model.reloadImageResources(authorizationManager.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (Integer deliveredCount) -> {
                            // обновление вьюхи, если были получены > 0 ресурсов
                            if (deliveredCount > 0 && view != null) {
                                view.reloadImages();
                            }
                            updateViewBasedOnResourcesCount(model.getImageResourcesCount());
                            stopReloading();
                        },
                        (Throwable error) -> {
                            updateViewBasedOnResourcesCount(model.getImageResourcesCount());
                            showError(error);
                            stopReloading();
                            // выход из аккаунта, если пользователь стал неавторизован
                            if (checkIfUserHasLoggedOut(error)) {
                                logout();
                                if (view != null) {
                                    view.showUserLogoutDialog();
                                }
                            }
                        }
                ));
    }

    private void startReloading() {
        if (view != null && model.getImageResourcesCount() == 0) {
            view.showGalleryView(false);
            view.showEmptyGalleryView(false);
            view.showProgressView(true);
        }
        isReloading = true;
    }

    private void setUpAuthorizationView() {
        if (view != null) {
            if (authorizationManager.isLoggedIn()) {
                view.setLogoutView();
            } else {
                view.setLoginView();
            }
        }
    }

    private void updateViewBasedOnResourcesCount(int count) {
        if (view != null) {
            view.showEmptyGalleryView(count == 0);
            view.showGalleryView(count != 0);
        }
    }

    private void stopReloading() {
        if (view != null) {
            view.showProgressView(false);
            view.stopRefreshing();
        }
        isReloading = false;
    }

    /**
     * Проверка, если пользователь стал неавторизованным, например,
     * изменил пароль в аккаунте.
     * @param error ошибка от сервера.
     * @return true, если пользователь стал неавторизован
     */
    private boolean checkIfUserHasLoggedOut(Throwable error) {
        if (error instanceof HttpException) {
            HttpException httpException = (HttpException) error;
            return httpException.code() == 401;
        }
        return false;
    }

    /**
     * Делегирование отображения сообщения об ошибке на вьюху, на основании полученого ответа от сервера.
     *
     * @param error ошибка при запросе.
     */
    @VisibleForTesting
    void showError(Throwable error) {
        if (view == null) {
            return;
        }
        if (error instanceof HttpException) {
            HttpException httpException = (HttpException) error;
            switch (httpException.code()) {
                case 400:
                    view.showIncorrectDataError();
                    break;
                case 401:
                    view.showNotAuthorizedError();
                    break;
                case 402:
                case 403:
                    view.showAccessDeniedError();
                    break;
                case 404:
                    view.showCannotFindResourceError();
                    break;
                case 406:
                    view.showWrongResourceFormatError();
                    break;
                case 429:
                    view.showTooManyRequestsError();
                    break;
                case 503:
                    view.showServiceUnavailableError();
                    break;
                default:
                    view.showUnknownError();
                    break;
            }
        } else {
            view.showInternetConnectionError();
        }
    }
}
