package com.deniszaplatnikov.imagegallery.ui.gallery;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.deniszaplatnikov.imagegallery.data.YandexDiskService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GalleryModel implements GalleryContract.Model {

    private final YandexDiskService yandexDiskService;
    private final List<ImageResource> imageResources = new ArrayList<>();
    private final int limit = 20;
    private int offset = 0;
    private boolean hasAnyResourcesToLoadMore = true;

    @Inject
    GalleryModel(YandexDiskService yandexDiskService) {
        this.yandexDiskService = yandexDiskService;
    }

    @Override
    public Observable<Integer> reloadImageResources(@NonNull String token) {
        offset = 0;
        // установка авторизационного заголовка и получение нового списка изображений на диске пользователя
        return yandexDiskService.getResources(String.format("OAuth %s", token),
                "image", "L", true, limit, offset)
                .map(getResourcesResponse -> {
                    int resourcesResponseSize = getResourcesResponse.getItems().size();

                    // очищение старых данных и добавление всех полученных ресурсов
                    imageResources.clear();
                    imageResources.addAll(getResourcesResponse.getItems());

                    // обновление сдвига относительно начала списка ресурсов
                    offset = resourcesResponseSize;
                    // заканчивать подгрузку, если полученное число ресурсов
                    // меньше запрашиваемого количество
                    hasAnyResourcesToLoadMore = resourcesResponseSize >= limit;

                    return resourcesResponseSize;
                });
    }

    @Override
    public Observable<Integer> loadMoreImageResources(@NonNull String token) {

        // установка авторизационного заголовка и дозагрузка списка изображений на диске пользователя
        return yandexDiskService.getResources(String.format("OAuth %s", token),
                "image", "L", true, limit, offset)
                .map(getResourcesResponse -> {
                    int resourcesResponseSize = getResourcesResponse.getItems().size();

                    // добавление полученных ресурсов в конец списка
                    imageResources.addAll(getResourcesResponse.getItems());

                    // обновление сдвига относительно начала списка ресурсов
                    offset += resourcesResponseSize;

                    // загрузка новых ресурсов прекращается,
                    // когда полученное число ресурсов
                    // меньше запрашиваемого количество
                    if (resourcesResponseSize < limit) {
                        hasAnyResourcesToLoadMore = false;
                    }

                    return resourcesResponseSize;
                });
    }

    @Override
    @Nullable
    public ImageResource getImageResource(int position) {
        return position >= 0 && position < imageResources.size() ? imageResources.get(position) : null;
    }

    @Override
    public int getImageResourcesCount() {
        return imageResources.size();
    }

    @Override
    public boolean hasAnyResourcesToLoadMore() {
        return hasAnyResourcesToLoadMore;
    }

    @Override
    public void clearResources() {
        imageResources.clear();
        offset = 0;
        hasAnyResourcesToLoadMore = true;
    }
}
