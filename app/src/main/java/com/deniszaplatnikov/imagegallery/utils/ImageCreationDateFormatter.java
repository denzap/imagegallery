package com.deniszaplatnikov.imagegallery.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

public class ImageCreationDateFormatter {

    /**
     * Форматирует строку с датой вида: 2018-04-06T12:50:01+00:00
     * в строку с датой вида: 06 апреля 2018, 15:50.
     * @param date строка с датой.
     * @return строка вида 06 апреля 2018, 15:50.
     */
    public static String formatDate(String date) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.getDefault());
        DateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy, HH:mm", Locale.getDefault());
        try {
            Date originalDate = originalFormat.parse(date);
            return targetFormat.format(originalDate);
        } catch (Exception ex) {
            Timber.d(ex);
            return "-";
        }
    }

}
