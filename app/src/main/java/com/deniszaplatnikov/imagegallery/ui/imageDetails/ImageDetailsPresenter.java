package com.deniszaplatnikov.imagegallery.ui.imageDetails;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.deniszaplatnikov.imagegallery.data.ImageResource;
import com.deniszaplatnikov.imagegallery.utils.ImageCreationDateFormatter;
import com.deniszaplatnikov.imagegallery.utils.fileSizeConverter.IFileSizeFormatter;

import javax.inject.Inject;

public class ImageDetailsPresenter implements ImageDetailsContract.Presenter {

    @Nullable private ImageDetailsContract.View view;
    @Nullable private ImageResource imageResource;
    private boolean isImageLoading = false;
    private final IFileSizeFormatter fileSizeFormatter;

    @Inject
    ImageDetailsPresenter(IFileSizeFormatter fileSizeFormatter) {
        this.fileSizeFormatter = fileSizeFormatter;
    }

    @Override
    public void attachView(ImageDetailsContract.View view, boolean isNew) {
        this.view = view;

        // восстановление вьюхи после пересоздания,
        // если информация об изображении уже имеется
        if (!isNew && imageResource != null) {
            setImageDetails(imageResource);
        }

        view.showProgressView(isNew || isImageLoading);
        view.showImageDetailsView(!isNew && !isImageLoading);
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public void stop() {

    }

    @Override
    public void setImageDetails(@NonNull ImageResource imageDetails) {
        imageResource = imageDetails;
        if (view != null) {
            isImageLoading = true;
            view.loadImage(imageDetails.getOriginalUrl());

            view.setImageName(imageDetails.getName());
            view.setImageMadeTime(ImageCreationDateFormatter.formatDate(imageDetails.getCreated()));
            view.setImageSize(fileSizeFormatter.formatFileSize(imageDetails.getSize()));
        }
    }

    @Override
    public void imageLoaded(boolean success) {
        isImageLoading = false;
        if (view != null) {
            view.showImage(success);
            view.showLoadImageError(!success);
            view.showImageDetailsView(true);
            view.showProgressView(false);
            view.stopReloadImageAnimation();
        }
    }

    @Override
    public void onSaveImageClicked() {
        if (imageResource != null && view != null) {
            view.saveImage(imageResource.getOriginalUrl());
        }
    }

    @Override
    public void onReloadImageClicked() {
        if (imageResource != null && view != null) {
            view.startReloadImageAnimation();
            view.loadImage(imageResource.getOriginalUrl());
        }
    }
}
