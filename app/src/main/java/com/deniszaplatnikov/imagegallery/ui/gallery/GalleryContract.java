package com.deniszaplatnikov.imagegallery.ui.gallery;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.deniszaplatnikov.imagegallery.ui.base.BasePresenter;
import com.deniszaplatnikov.imagegallery.ui.base.BaseView;
import com.deniszaplatnikov.imagegallery.data.ImageResource;

import io.reactivex.Observable;

public interface GalleryContract {

    interface Model {

        /**
         * Очистка и получение самого свежего списка ресурсов изображений.
         * @param token авторизационный токен.
         * @return количество ресурсов,
         * которое было получено в результате операции.
         */
        Observable<Integer> reloadImageResources(@NonNull String token);

        /**
         * Загрузка следующей части списка ресурсов изображений.
         * @param token авторизационный токен.
         * @return количество ресурсов,
         * которое было получено в результате операции.
         */
        Observable<Integer> loadMoreImageResources(@NonNull String token);

        /**
         * Получение ресурса изображения, хранящего на заданной position.
         * Возможно получение null, если position находится вне границ хранилища.
         * @param position индекс ресурса.
         * @return ресурс изображения, если такой найден.
         */
        @Nullable ImageResource getImageResource(int position);

        /**
         * Получение количества всех загруженных ресурсов изображений.
         * @return количество загруженных ресурсов.
         */
        int getImageResourcesCount();

        /**
         * Индикатор того, есть ли еще возможность загрузить следующую часть
         * списка ресурсов изображений.
         * @return true, если загружены еще не все ресурсы.
         */
        boolean hasAnyResourcesToLoadMore();

        /**
         * Удаление всех данных о загруженных ресурсах из хранилища.
         */
        void clearResources();
    }

    interface View extends BaseView {

        /**
         * Установка вьюхи, ответственной за отображение
         * состояния авторизованного пользователя.
         */
        void setLoginView();

        /**
         * Установка вьюхи, ответственной за отображение
         * состояния неавторизованного пользователя.
         */
        void setLogoutView();

        /**
         * Обновление списка изображений.
         */
        void reloadImages();

        /**
         * Осуществление перехода на отображение информации
         * о конкретном ресурсе изображения.
         * @param imageResource ресурс изображения для отображения.
         */
        void navigateToImageDetailsView(@NonNull ImageResource imageResource);

        /**
         * Осуществление перехода к авторизации пользователя.
         */
        void navigateToAuthorizationView();

        /**
         * Отображение диалога о выходе из аккаунта пользователя.
         */
        void showAcceptLogoutDialog();

        /**
         * Отображение информационного сообщения о том,
         * что пользователь вышел из аккаунта.
         */
        void showUserLogoutDialog();

        /**
         * Показать или спрятать экран загрузки.
         * @param show true, чтобы показать, false, чтобы спрятать.
         */
        void showProgressView(boolean show);

        /**
         * Показать или спрятать placeholder для пустой галереи.
         * @param show true, чтобы показать, false, чтобы спрятать.
         */
        void showEmptyGalleryView(boolean show);

        /**
         * Показать или спрятать список изображений в галерее.
         * @param show true, чтобы показать, false, чтобы спрятать.
         */
        void showGalleryView(boolean show);

        /**
         * Остановка индикации pull-to-refresh.
         */
        void stopRefreshing();

        /**
         * Показ ошибки авторизации, если пользователь отказался предоставлять права на чтение диска.
         */
        void showAuthorizationAccessDeniedError();

        /**
         * Показ ошибки авторизации, если используется неавторизованный клиент.
         */
        void showUnauthorizedClientError();

        /**
         * Показ ошибки авторизации, если авторизационный токен не удалось распознать.
         */
        void showCannotReceiveAuthToken();

        /**
         * Показ ошибки, если заданы некорректные данные запроса на получение изображений.
         */
        void showIncorrectDataError();

        /**
         * Показ ошибки, если пользователь не авторизован.
         */
        void showNotAuthorizedError();

        /**
         * Показ ошибки, если доступ запрещён.
         * Возможно, у приложения недостаточно прав для данного действия.
         */
        void showAccessDeniedError();

        /**
         * Показ ошибки, если не удалось найти запрошенный ресурс.
         */
        void showCannotFindResourceError();

        /**
         * Показ ошибки, если ресурс не может быть представлен в запрошенном формате.
         */
        void showWrongResourceFormatError();

        /**
         * Показ ошибки, если было произведено слишком много запросов.
         */
        void showTooManyRequestsError();

        /**
         * Показ ошибки, если сервис временно недоступен.
         */
        void showServiceUnavailableError();

        /**
         * Показ ошибки, если проблемы с интернетом.
         */
        void showInternetConnectionError();

        /**
         * Показ неизвестной ошибки.
         */
        void showUnknownError();

        /**
         * Получение данных о доступности сети.
         */
        boolean getNetworkAvailable();
    }

    interface ImageViewCell {

        /**
         * Загрузка изображения по заданному url.
         * @param url для загрузки изображения.
         */
        void loadImage(@NonNull String url);
    }

    interface Presenter extends BasePresenter<View> {

        /**
         * Добавление вьюхи, если дополнительно ей был передан результат авторизации пользователя.
         * @param view вьюха.
         * @param isNew созданная/пересозданная.
         * @param tokenData данные об авторизационном токене.
         */
        void attachViewWithToken(@NonNull GalleryContract.View view, boolean isNew, @NonNull String tokenData);

        /**
         * Оповещение об инициации перезагрузки списка изображений.
         */
        void onRefreshGallery();

        /**
         * Оповещение о том, что был достигнут конец списка изображений.
         */
        void onEndOfGallery();

        /**
         * Установка данных об изображении в соответствующую ячейку в списке изображений.
         * @param imageViewCell ячейка изображения.
         * @param position позиция ячейки в списке.
         */
        void onBindImageViewCell(@NonNull ImageViewCell imageViewCell, int position);

        /**
         * Получение количества всех загруженных ресурсов изображений.
         * @return количество загруженных ресурсов.
         */
        int getImageResourcesCount();

        /**
         * Оповещение о клике по изображению в определенной позиции списка.
         * @param position позиция изображения в списке.
         */
        void imageClicked(int position);

        /**
         * Оповещение о запросе на изменение состояния авторизации.
         */
        void onChangeAuthorizationState();

        /**
         * Оповещение о том, что выход из аккаунта был подтвержден.
         */
        void logoutAccepted();
    }
}
