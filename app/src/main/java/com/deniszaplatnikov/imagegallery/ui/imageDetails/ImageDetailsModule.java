package com.deniszaplatnikov.imagegallery.ui.imageDetails;

import com.deniszaplatnikov.imagegallery.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ImageDetailsModule {

    @Binds
    @ActivityScoped
    public abstract ImageDetailsContract.Presenter bindImageDetailsPresenter(ImageDetailsPresenter presenter);
}
