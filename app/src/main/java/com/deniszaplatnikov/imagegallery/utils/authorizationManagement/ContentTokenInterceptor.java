package com.deniszaplatnikov.imagegallery.utils.authorizationManagement;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;

public final class ContentTokenInterceptor implements Interceptor {

    private String contentToken = null;

    public void setContentToken(String contentToken) {
        this.contentToken = contentToken;
    }

    @Override
    public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
        // Добление авторизационный токен, чтобы Picasso мог загружать картинки
        Request newRequest = chain.request().newBuilder()
                .addHeader("Authorization", "OAuth " + contentToken)
                .build();
        return chain.proceed(newRequest);
    }
}