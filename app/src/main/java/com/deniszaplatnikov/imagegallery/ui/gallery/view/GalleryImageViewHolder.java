package com.deniszaplatnikov.imagegallery.ui.gallery.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.deniszaplatnikov.imagegallery.ImageGalleryApplication;
import com.deniszaplatnikov.imagegallery.R;
import com.deniszaplatnikov.imagegallery.ui.gallery.GalleryContract;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GalleryImageViewHolder extends RecyclerView.ViewHolder implements GalleryContract.ImageViewCell {

    private final GalleryContract.Presenter presenter;

    @BindView(R.id.image) ImageView image;

    GalleryImageViewHolder(View itemView, GalleryContract.Presenter presenter) {
        super(itemView);

        this.presenter = presenter;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void loadImage(@NonNull String url) {
        ImageGalleryApplication.getPicasso().load(url).into(image);
    }

    @OnClick(R.id.card_view_image)
    public void onImageClicked() {
        presenter.imageClicked(getAdapterPosition());
    }
}
