package com.deniszaplatnikov.imagegallery.ui.gallery.view;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.deniszaplatnikov.imagegallery.R;
import com.deniszaplatnikov.imagegallery.ui.authorization.AuthorizationActivity;
import com.deniszaplatnikov.imagegallery.ui.imageDetails.ImageDetailsActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class GalleryActivityTest {

    @Rule public ActivityTestRule<GalleryActivity> activityTestRule = new ActivityTestRule<>(GalleryActivity.class);

    @Test
    public void loginInitialState() {
        onView(withId(R.id.toolbar_title)).check(matches(withText(R.string.general_feed)));
        onView(withId(R.id.right_item)).check(matches(isDisplayed()));
        onView(withId(R.id.right_item_text)).check(matches(isDisplayed()));
        onView(withId(R.id.right_item_text)).check(matches(withText(R.string.login)));
        onView(withId(R.id.right_item_image)).check(matches(isDisplayed()));
    }

    public void navigateOnImageClicked() {
        Intents.init();
        onView(withId(R.id.recycler_image_gallery))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        intended(hasComponent(ImageDetailsActivity.class.getName()));
        Intents.release();
    }

    public void onLoginClicked() {
        Intents.init();
        onView(withId(R.id.right_item)).perform(click());
        intended(hasComponent(AuthorizationActivity.class.getName()));
        Intents.release();
    }

}