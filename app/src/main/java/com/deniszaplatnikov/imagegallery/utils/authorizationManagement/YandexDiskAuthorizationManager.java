package com.deniszaplatnikov.imagegallery.utils.authorizationManagement;

import android.content.SharedPreferences;

import javax.inject.Inject;

public class YandexDiskAuthorizationManager implements AuthorizationManager {

    private final static String IS_AUTHORIZED_PREF = "is_authorized";
    private final static String TOKEN_PREF = "token";
    private final static String DEFAULT_TOKEN = "AQAAAAAL14S6AATuvJsvWV9PEkHbo6twLGV8MPM";

    private final SharedPreferences sharedPreferences;
    private final ContentTokenInterceptor contentTokenInterceptor;

    @Inject
    public YandexDiskAuthorizationManager(SharedPreferences sharedPreferences, ContentTokenInterceptor contentTokenInterceptor) {
        this.sharedPreferences = sharedPreferences;
        this.contentTokenInterceptor = contentTokenInterceptor;
        contentTokenInterceptor.setContentToken(getToken());
    }

    @Override
    public void login(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN_PREF, token);
        editor.putBoolean(IS_AUTHORIZED_PREF, true);
        editor.apply();

        contentTokenInterceptor.setContentToken(token);
    }

    @Override
    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_AUTHORIZED_PREF, false);
    }

    @Override
    public String getToken() {
        return sharedPreferences.getString(TOKEN_PREF, DEFAULT_TOKEN);
    }

    @Override
    public void logout() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(IS_AUTHORIZED_PREF);
        editor.remove(TOKEN_PREF);
        editor.apply();

        contentTokenInterceptor.setContentToken(DEFAULT_TOKEN);
    }
}
